program wave
use types, only: dp
use utils, only: stop_error, assert
implicit none

real(dp), allocatable :: y(:), yp1(:), ym1(:) ! y(m), x=mX
real(dp), allocatable :: yt(:), ydt(:) ! yt(n), t=nT
real(dp) :: X, T, a10, a11, a12, a20, a21
real(dp) :: lambda, mu, kappa, b1, b2, L, c, Fs, d, h, Tmax, Tend
real(dp) :: sigma
integer :: Nx, Nt, m, n, u, Nskip

Nx = 200
Tend = 25e-3_dp ! s
Tend = 10

! C2
L = 1.23_dp ! m
c = 160.9_dp ! m / s
kappa = 0.58_dp ! m^2 / s
b1 = 0.25_dp ! 1/s
b2 = 7.5e-5_dp ! m^2 / s
Fs = 16000_dp ! 1/s

! C4
!L = 0.63_dp ! m
!c = 329.6_dp ! m / s
!kappa = 1.25_dp ! m^2 / s
!b1 = 1.1_dp ! 1/s
!b2 = 2.7e-4_dp ! m^2 / s
!Fs = 32000_dp ! 1/s

d = L/8
h = 1e-3_dp ! m
sigma = 0.03_dp

X = L / Nx
Tmax = X**2 * (-4*b2+sqrt(16*b2**2+4*(c**2*X**2+4*kappa**2))) &
    / (2*(c**2*X**2+4*kappa**2))
T = Tmax * 0.8_dp ! 80% of the maximum stable time step
T = 1._dp / 44100
Nskip = 1
T = T / Nskip
Nt = int(Tend / T) + 1


print *, "Nt =", Nt
print *, "Stability condition: T <= Tmax"
print *, "Tmax =", Tmax
print *, "T    =", T
call assert(T <= Tmax)

allocate(y(Nx), yp1(Nx), ym1(Nx), yt(Nt), ydt(Nt))

lambda = c*T/X
mu = kappa*T/X**2
a10 = (2 - 2*lambda**2 - 6*mu**2 - 4*b2*T/X**2) / (1 + b1*T)
a11 = (lambda**2 + 4*mu**2 + 2*b2*T/X**2) / (1 + b1*T)
a12 = -mu**2 / (1 + b1*T)
a20 = (-1 + 4*b2*T/X**2 + b1*T) / (1 + b1*T)
a21 = (-2*b2*T/X**2) / (1 + b1*T)

! IC
!do m = 1, Nx
!    if (m*X < d) then
!        y(m,1) = h*m*X/d
!        y(m,2) = h*m*X/d
!    else
!        y(m,1) = h*(L-m*X) / (L-d)
!        y(m,2) = h*(L-m*X) / (L-d)
!    end if
!end do
!do m = 1, Nx
!    y(m,1) = h*exp(-(m*X-d)**2/sigma**2)
!    y(m,2) = y(m,1)
!end do
do m = 1, Nx
    ym1(m) = 0
    y(m) = h*exp(-(m*X-d)**2/sigma**2)/10 * T / 1.9842966907337461E-005_dp
end do
! BC
y(:2) = 0
y(Nx-1:) = 0
ym1(:2) = 0
ym1(Nx-1:) = 0

! TODO: reconcile IC and BC

! Time propagation
yt(1) = ym1(9*Nx/10)
yt(2) = y(9*Nx/10)
do n = 2, Nt-1
    yp1(:2) = 0
    yp1(Nx-1:) = 0
    do m = 3, Nx-2
        yp1(m) = a10*y(m)+a11*(y(m+1)+y(m-1))+a12*(y(m+2)+y(m-2))+&
            a20*ym1(m)+a21*(ym1(m+1)+ym1(m-1))
    end do
    yt(n+1) = yp1(9*Nx/10)

    ym1 = y
    y = yp1
end do
ydt = (yt(3:Nt) - yt(1:Nt-2)) / (2*T)

print *, "Done, saving..."

open(newunit=u, file="data.txt", status="replace")
write(u, *) Nx, Nt, X, T, Nskip
close(u)

open(newunit=u, file="y.txt", status="replace")
write(u, *) yt(::Nskip)
close(u)

open(newunit=u, file="yt.txt", status="replace")
write(u, *) ydt(::Nskip)
close(u)

print *, "Done"


contains

    real(dp) pure function l2norm(x, dx) result(r)
    real(dp), intent(in) :: x(:), dx
    r = sqrt(dx*sum(x**2))
    end function

end program

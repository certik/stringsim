# String Simulation

# References

[1] Bensa, J., Bilbao, S., Kronland-Martinet, R., & Smith, J. O. (2003). The
simulation of piano string vibration: from physical models to finite difference
schemes and digital waveguides. The Journal of the Acoustical Society of
America, 114(2), 1095–1107. http://doi.org/10.1121/1.1587146
